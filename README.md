# 前言
原理参考：[如何将24位RGB颜色转换16位RGB颜色](https://blog.csdn.net/baidu_26678247/article/details/65629447)  
0xFF0000 转为 0xF800  
核心就是 11111111 00000000 00000000  
8位R值右移3位，变为了5位：11111  
8位G值右移2位，变成了6位：000000  
8位B值右移3位，变成了5位：00000  
最后拼接回去 1111 1000 0000 0000  
OK，那么直接上效果和代码  
## 在线测试
[码云pages](https://ikaros-521.gitee.io/js-24rgb-to-16rgb/)

# 效果图
![在这里插入图片描述](https://img-blog.csdnimg.cn/3b2d4d65516f41c4a7f8642acf4b53c8.gif#pic_center)
